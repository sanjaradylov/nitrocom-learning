"""
Utilities for choosing a set of optimal hyperparameters for
a learning algorithm.
"""

import abc
import collections
import itertools

import numpy as np

from sklearn.utils import check_random_state

from nitrocomchem.splitter import KFoldSplitter


class HyperparameterOptimizer(metaclass=abc.ABCMeta):
    """Hyperparameter optimizer abstract base class.

    Parameters
    ----------
    estimator : deepchem.models.TensorGraph
        Statistical learning model.
    parameter_distributions_init : dict-like, default: None
        Hyperparameter distribution map, where keys are hyperparameters and
        values are allowable values.
        This map is for initializing the models (layer size, number of features
        etc.).
    parameter_distributions_fit : dict-like, default: None
        Hyperparameter distribution map, where keys are hyperparameters and
        values are allowable values.
        This map is for fitting the models (batch size, learning rate, etc.).
    splitter : nitrocomchem.splitter.KFoldSplitter or its subclass.
        Train-test splitter.
    scorer : callable, scikit-learn metrics
        Statistical metric.
    n_jobs : int, default 1
        Number of jobs to run in parallel.
        `-1` means using all processors.
    random_state : int, default: None (random number)
        Random state/seed.

    Abstract methods
    ----------------
    sample
        The way of generating hyperparameters.
    optimize
        The way of performing hyperparameter search.
    """

    def __init__(self,
                 estimator,
                 scorer,
                 parameter_distributions_init=None,
                 parameter_distributions_fit=None,
                 splitter=None,
                 n_jobs=1,
                 random_state=None,
                 **others):
        random_state = random_state or int(np.random.uniform() * (2**31 - 1))
        splitter = splitter or KFoldSplitter(random_state=random_state)
        parameter_distributions_init = parameter_distributions_init or {}
        parameter_distributions_fit = parameter_distributions_fit or {}

        self._check_parameters(
            estimator=estimator,
            scorer=scorer,
            parameter_distributions_init=parameter_distributions_init,
            parameter_distributions_fit=parameter_distributions_fit,
            splitter=splitter,
            n_jobs=n_jobs,
            random_state=random_state,
            **others,
        )

        self.estimator = estimator
        self.scorer = scorer
        self.parameter_distributions_init = parameter_distributions_init
        self.parameter_distributions_fit = parameter_distributions_fit
        self.n_jobs = n_jobs
        self.splitter = splitter
        self.random_state = random_state

    @classmethod
    def _check_parameters(cls,
                          *,
                          estimator,
                          parameter_distributions_init,
                          parameter_distributions_fit,
                          splitter,
                          scorer,
                          n_jobs,
                          random_state,
                          **others):
        assert parameter_distributions_init or parameter_distributions_fit,\
            'At least one of the parameter_distribution variables must be ' \
            'specified.'
        assert hasattr(estimator, 'fit'),\
            'Estimator {0!r} must implement `fit` method.'.format(estimator)
        assert isinstance(parameter_distributions_init, dict), \
            'Parameter distributions must be a dict-like ' \
            'with keys as hyperparameters and ' \
            'values as corresponding distributions.'
        assert isinstance(parameter_distributions_fit, dict), \
            'Parameter distributions must be a dict-like ' \
            'with keys as hyperparameters and ' \
            'values as corresponding distributions.'
        assert hasattr(splitter, 'split'),\
            'Splitter {0!r} must implement `split` method.'.format(splitter)
        assert hasattr(scorer, '__call__'), 'Scorer must be callable.'
        assert n_jobs == -1 or n_jobs > 0, \
            'Number of parallel jobs `n_jobs` must be -1 (all CPUs) or ' \
            'positive.'
        assert random_state >= 0, 'Random state must be a positive integer.'

    @abc.abstractmethod
    def sample(self):
        """Generate hyperparameter samples.

        Returns
        -------
        sample_parameters : iterator of dict-like objects
            Sampled hyperparameters.
        """

    @abc.abstractmethod
    def optimize(self, data_set, **others):
        """Optimize hyperparameters.

        Parameters
        ----------
        data_set : deepchem.data.DiskDataset
            Input objects and output values.
        """


class GridSearchOptimizer(HyperparameterOptimizer):
    """Exhaustive searching through the specified subset of
    the hyperparameter space.
    """

    def sample(self):
        """Generate hyperparameters from the grid.

        Yields
        ------
        samples : tuple
            sample_parameters_init : dict
                Hyperparameters for model initialization.
            sample_parameters_fit : dict
                Hyperparameters for model fitting.
        """
        keys_init, values_init = zip(
            *self.parameter_distributions_init.items()
        )
        keys_fit, values_fit = zip(*self.parameter_distributions_fit.items())

        for sample_values_init in itertools.product(*values_init):
            for sample_values_fit in itertools.product(*values_fit):
                sample_parameters_init = dict(
                    zip(keys_init, sample_values_init)
                )
                sample_parameters_fit = dict(
                    zip(keys_fit, sample_values_fit)
                )

                yield sample_parameters_init, sample_parameters_fit

    def optimize(self,
                 data_set,
                 task_names=None,
                 transformers=None):
        """Apply model evaluation for every hyperparameter set from the grid.

        Parameters
        ----------
        data_set : deepchem.data.DiskDataset
            Data set.
        task_names : list-like, default: None
            Optional task names (for convenience).
        transformers : list-like, default: None
            Deepchem transformers applied to data_set.

        Returns
        -------
        statistics : tuple
            cv_results : dict
                The grid containing hyperparameters and the cv scores
                calculated with the corresponding models.
            best_init_params : dict
                The best hyperparameters for model initialization.
                Hint: model.set_params(**best_init_params).
            best_fit_params : dict
                The best hyperparameters for model fitting.
                Hint: model.fit(**best_fit_params).
        """
        task_names = task_names or list(range(data_set.y.shape[1]))
        transformers = transformers or list()

        cv_results = collections.defaultdict(list)

        for sample_parameters_init, sample_parameters_fit in self.sample():

            for sample in sample_parameters_init, sample_parameters_fit:

                for key, value in sample.items():
                    cv_results[key].append(value)

            cv_results['init_parameters'].append(sample_parameters_init)
            cv_results['fit_parameters'].append(sample_parameters_fit)

            self.estimator.set_params(**sample_parameters_init)

            cv_errors = []

            for train_data, valid_data in self.splitter.split(data_set):

                self.estimator.fit(train_data, **sample_parameters_fit)
                predictions = self.estimator.predict(
                    valid_data,
                    transformers=transformers,
                )

                if data_set.y.ndim > 1:
                    cv_errors.append(self.scorer(valid_data.y, predictions,
                                                 multioutput='raw'))
                else:
                    cv_errors.append(self.scorer(valid_data.y, predictions))

            if data_set.y.ndim > 1:
                mean_scores = np.mean(cv_errors, axis=0)
                std_scores = np.std(cv_errors, axis=0)
                results = {
                    'mean_valid_score': np.mean(mean_scores),
                    'std_valid_score': np.mean(std_scores),
                }
                results.update(zip(
                    ['mean_{0}_score'.format(task) for task in task_names],
                    mean_scores,
                ))
                results.update(zip(
                    ['std_{0}_score'.format(task) for task in task_names],
                    std_scores,
                ))
            else:
                results = {
                    'mean_valid_score': np.mean(cv_errors),
                    'std_valid_score': np.std(cv_errors),
                }
            for key, value in results.items():
                cv_results[key].append(value)

        best_index = np.argmin(cv_results['mean_valid_score'])
        best_init_params = cv_results['init_parameters'][best_index]
        best_fit_params = cv_results['fit_parameters'][best_index]

        return cv_results, best_init_params, best_fit_params


class RandomSearchOptimizer(GridSearchOptimizer):
    """Random searching through the hyperparameter space.
    """

    def __init__(self,
                 estimator,
                 scorer,
                 parameter_distributions_init=None,
                 parameter_distributions_fit=None,
                 splitter=None,
                 n_jobs=1,
                 random_state=None,
                 n_iterations=1):
        super().__init__(
            estimator=estimator,
            scorer=scorer,
            parameter_distributions_init=parameter_distributions_init,
            parameter_distributions_fit=parameter_distributions_fit,
            splitter=splitter,
            n_jobs=n_jobs,
            random_state=random_state,
            n_iterations=n_iterations,
        )

        self.n_iterations = n_iterations

    @classmethod
    def _check_parameters(cls,
                          *,
                          estimator,
                          scorer,
                          parameter_distributions_init,
                          parameter_distributions_fit,
                          splitter,
                          n_jobs,
                          random_state,
                          n_iterations=1):
        super()._check_parameters(
            estimator=estimator,
            scorer=scorer,
            parameter_distributions_init=parameter_distributions_init,
            parameter_distributions_fit=parameter_distributions_fit,
            splitter=splitter,
            n_jobs=n_jobs,
            random_state=random_state,
        )

        assert n_iterations > 0, 'Number of iterations must be positive.'

    def sample(self):
        """Sample hyperparameters `n_iterations` times from the specified
        parameter distributions.

        Yields
        ------
        samples : tuple
            sample_parameters_init : dict
                Hyperparameters for model initialization.
            sample_parameters_fit : dict
                Hyperparameters for model fitting.
        """
        rng = check_random_state(self.random_state)

        for _ in range(self.n_iterations):

            sample_parameters_init, sample_parameters_fit = {}, {}

            for key, values in self.parameter_distributions_init.items():
                if hasattr(values, 'rvs'):
                    sample_parameters_init[key] = values.rvs(random_state=rng)
                else:
                    sample_parameters_init[key] = values[
                        np.random.randint(len(values))
                    ]

            for key, values in self.parameter_distributions_fit.items():
                if hasattr(values, 'rvs'):
                    sample_parameters_fit[key] = values.rvs(random_state=rng)
                else:
                    sample_parameters_fit[key] = values[
                        np.random.randint(len(values))
                    ]

            yield sample_parameters_init, sample_parameters_fit
