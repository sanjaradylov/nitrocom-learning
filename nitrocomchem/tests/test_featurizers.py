import unittest

import numpy as np

from rdkit import Chem

from nitrocomchem.featurizer import CoulombMatrixFeaturizer


class TestCaseCoulombMatrixFeaturizer(unittest.TestCase):
    def setUp(self):
        self.featurizer = CoulombMatrixFeaturizer(
            max_atoms=50,
            remove_hydrogens=False,
            randomize=False,
            seed=0,
        )
        self.smiles_strings = [
            r'O1C=C[C@H]([C@H]1O2)c3c2cc(OC)c4c3OC(=O)C5=C4CCC(=O)5',
            r'OC[C@@H](O1)[C@@H](O)[C@H](O)[C@@H](O)[C@@H](O)1',
            r'CC(=O)OCCC(/C)=C\C[C@H](C(C)=C)CCC=C',
        ]
        self.molecules = list(map(Chem.MolFromSmiles, self.smiles_strings))

    def test1_rdkit_molecules(self):
        self.assertTrue(
            all(molecule is not None for molecule in self.molecules)
        )

    def test2_matrices(self):
        features = self.featurizer.featurize(
            self.molecules,
            verbose=False,
        )
        features = np.asarray(features)

        self.assertEqual(features.ndim, 4)
        self.assertEqual(features.shape[0], len(self.smiles_strings))
        self.assertEqual(features.shape[1], 1)
        self.assertEqual(
            features.shape[2],
            features.shape[3],
            self.featurizer.max_atoms
        )


if __name__ == '__main__':
    unittest.main()
