import doctest
import unittest

from nitrocomchem.splitter import splitters


def main():
    suite = unittest.TestSuite()
    suite.addTest(doctest.DocTestSuite(module=splitters))

    runner = unittest.TextTestRunner()
    print(runner.run(suite))


if __name__ == '__main__':
    main()
