import collections
import random
import string
import unittest

from nitrocomchem.utils import (
    SortedDict,
    SortedList,
)


class TestCaseSortedList(unittest.TestCase):
    def setUp(self):
        self.n_items = 10
        self.sequence = list(range(self.n_items))
        random.shuffle(self.sequence)

        self.sorted_sequence = SortedList(self.sequence)

        self.item = 3.5

    def test1_delegated_magic_methods(self):
        self.assertCountEqual(self.sequence, self.sorted_sequence)

        self.assertEqual(str(sorted(self.sequence)), str(self.sorted_sequence))

        self.assertEqual(
            list(reversed(self.sorted_sequence)),
            sorted(self.sequence, reverse=True),
        )

    def test2_insert_and_contains(self):
        self.assertNotIn(self.item, self.sorted_sequence)
        self.sorted_sequence.insert(self.item)
        self.assertIn(self.item, self.sorted_sequence)

    def test3_pop_and_remove(self):
        self.assertEqual(self.sorted_sequence[-1], self.sorted_sequence.pop())

        with self.assertRaises(ValueError):
            self.sorted_sequence.remove(self.item)

        item = 10
        self.sorted_sequence.insert(item)
        self.sorted_sequence.remove(item)
        self.assertNotIn(item, self.sorted_sequence)

    def test4_index(self):
        less_than_min = min(self.sorted_sequence) - 1
        self.sorted_sequence.insert(less_than_min)
        self.assertEqual(self.sorted_sequence.index(less_than_min), 0)

        greater_than_max = max(self.sorted_sequence) + 1
        self.sorted_sequence.insert(greater_than_max)
        self.assertEqual(
            self.sorted_sequence.index(greater_than_max),
            len(self.sorted_sequence) - 1,
        )

        mean = (less_than_min + greater_than_max) / 2
        self.sorted_sequence.insert(mean)
        self.assertEqual(
            self.sorted_sequence.index(mean),
            len(self.sorted_sequence) // 2,
        )

    def test5_count(self):
        item = 2
        sequence = [item, item - 1, item + 1]
        sorted_sequence = SortedList(sequence)
        self.assertEqual(1, sorted_sequence.count(item))

        n_items = 9
        sequence += [item] * n_items
        sorted_sequence = SortedList(sequence)
        self.assertEqual(n_items + 1, sorted_sequence.count(item))

    def test6_extend(self):
        random_sequence = [random.randint(-100, 100) for _ in range(10)]
        self.sorted_sequence.extend(random_sequence)
        self.sequence.extend(random_sequence)

        self.assertTrue(self.sorted_sequence == sorted(self.sequence))

    def test_edge_cases(self):
        sorted_sequence = SortedList()

        self.assertFalse(SortedList(sorted_sequence))
        self.assertFalse(SortedList(set()))
        self.assertFalse(SortedList(tuple()))
        self.assertFalse(sorted_sequence)
        self.assertNotIn(10, sorted_sequence)
        self.assertEqual(0, len(sorted_sequence))
        self.assertEqual(0, sorted_sequence.count(10))

        with self.assertRaises(IndexError):
            sorted_sequence.pop()

    def test_nontrivial_key_function(self):
        TestDataType = collections.namedtuple(
            'TestDataType',
            'int_field str_field',
        )

        items = [
            TestDataType._make((
                random.randint(-10, 10),
                random.choice(string.ascii_letters)
            ))
            for _ in range(20)
        ]
        sorted_items = SortedList(
            sequence=items,
            key_function=lambda item: item.str_field.lower(),
        )
        for x, y in zip(sorted_items[:-1], sorted_items[1:]):
            self.assertLessEqual(x.str_field.lower(), y.str_field.lower())

    def tearDown(self):
        self.sorted_sequence.clear()
        self.sequence.clear()


class TestCaseSortedDict(unittest.TestCase):
    def setUp(self):
        self.n_items = len(string.ascii_lowercase)

        self.keys = list(string.ascii_lowercase)
        self.values = [random.randint(-20, 20) for _ in range(self.n_items)]

        random.shuffle(self.keys)

        self.sorted_dict = SortedDict(dict(zip(self.keys, self.values)))

    def test1_init(self):
        self.assertSequenceEqual(
            list(self.sorted_dict.keys()),
            sorted(self.keys),
        )
        self.assertSequenceEqual(
            list(self.sorted_dict.values()),
            [v for k, v in sorted(zip(self.keys, self.values))]
        )

    def test2_iter(self):
        previous_key = next(self.sorted_dict.keys())

        for key in self.sorted_dict:
            self.assertLessEqual(previous_key, key)
            previous_key = key

    def test3_items(self):
        self.assertEqual(self.n_items, len(self.sorted_dict))

        for key, value in self.sorted_dict.items():
            self.assertEqual(self.sorted_dict[key], value)

    def test4_remove(self):
        key = random.choice(self.keys)
        value = self.sorted_dict[key]

        self.assertEqual(value, self.sorted_dict.pop(key))
        self.assertNotIn(key, self.sorted_dict)

        with self.assertRaises(KeyError):
            self.sorted_dict.pop(key)

        self.assertEqual('dummy', self.sorted_dict.pop(key, 'dummy'))

        key, value = self.sorted_dict.popitem()
        self.assertIn(key, self.keys)
        self.assertIn(value, self.values)

    def test5_upgrade(self):
        new_dictionary = SortedDict.fromkeys(string.digits, value=3.14)
        self.sorted_dict.update(new_dictionary)

        self.assertSequenceEqual(
            list(self.sorted_dict.keys()),
            sorted(string.ascii_lowercase + string.digits),
        )

    def test_edge_cases(self):
        sorted_dict = SortedDict()

        self.assertFalse(sorted_dict)
        self.assertEqual(0, len(sorted_dict))

        self.assertCountEqual([], list(sorted_dict.keys()))

        with self.assertRaises(KeyError):
            sorted_dict.pop('dummy')
            sorted_dict.popitem()

    def tearDown(self):
        self.sorted_dict.clear()


if __name__ == '__main__':
    unittest.main()
