import unittest

import numpy as np

from nitrocomchem.transformer import YeoJohnsonTransformer


class TestCaseYeoJohnsonTransformer(unittest.TestCase):
    def setUp(self):
        self.data = np.array([
            [1., 2.],
            [3., 2.],
            [4., 5.],
        ])

        self.lambdas = np.array([1.38668178, -3.10053309])
        self.data_transformed = np.array([
            [-1.31616039, -0.70710678],
            [0.20998268, -0.70710678],
            [1.1061777, 1.41421356],
        ])

        # "Unresolved attribute reference" appears because of the documentation
        # bug in scikit-learn's validation utilities.
        self.transformer = YeoJohnsonTransformer().fit(self.data)
        self.data_transformed_ = self.transformer.transform(self.data)
        self.data_ = self.transformer.inverse_transform(self.data_transformed_)

    def test1_lambdas(self):
        np.testing.assert_array_almost_equal(
            self.lambdas,
            self.transformer.lambdas_,
            decimal=6,
            verbose=True,
        )

    def test2_transform(self):
        np.testing.assert_array_almost_equal(
            self.data_transformed,
            self.data_transformed_,
            decimal=6,
            verbose=True,
        )

    def test3_inverse_transform(self):
        np.testing.assert_array_almost_equal(
            self.data,
            self.data_,
            decimal=6,
            verbose=True,
        )


if __name__ == '__main__':
    unittest.main()
