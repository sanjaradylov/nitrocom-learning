import doctest
import unittest

from nitrocomchem.featurizer import selectors


def main():
    suite = unittest.TestSuite()
    suite.addTest(doctest.DocTestSuite(module=selectors))

    runner = unittest.TextTestRunner()
    print(runner.run(suite))


if __name__ == '__main__':
    main()
