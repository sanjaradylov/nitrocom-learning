"""
Train-test splitter objects.

Classes
-------
KFoldSplitter
    K-fold strategy.

RepeatedKFoldSplitter
    Repeated k-fold cross-validation.

LeaveOneOutSplitter
    Leave-one-out strategy.

"""

import deepchem as dc
import numpy as np

from sklearn.utils import check_random_state


class RepeatedKFoldSplitter:
    """Splits data into training and validation tests repeatedly
    using k-fold strategy.

    Parameters
    ----------
    n_repeats : int, default: 3
        Number of times to repeat k-fold cross-validation.
    n_folds : int, default: 5
        Number of folds.
    random_state : int, default: None
        Random seed.
    """
    def __init__(self,
                 n_repeats=3,
                 n_folds=5,
                 random_state=None):
        self.n_repeats = n_repeats
        self.n_folds = n_folds
        self.random_state = random_state

    def __repr__(self):
        return (
            '{0.__class__.__name__}(\n'
            '    n_repeats={0.n_repeats},\n'
            '    n_folds={0.n_folds},\n'
            '    random_state={0.random_state}\n)'
            .format(self)
        )

    def split(self, data_set):
        """Repeatedly generate training and validation sets.

        Parameters
        ----------
        data_set : deepchem.data.DiskDataset
            Input objects and output values.

        Yields
        -------
        sets : generator of tuples
            data_train : deepchem.data.DiskDataset
                Training data set.
            data_valid : deepchem.data.DiskDataset
                Validation data set.
        """
        rng = check_random_state(self.random_state)

        for _ in range(self.n_repeats):
            kfold_cv = KFoldSplitter(n_folds=self.n_folds,
                                     shuffle=True, random_state=rng)

            yield from kfold_cv.split(data_set)

    @property
    def n_repeats(self):
        return self._n_repeats

    @n_repeats.setter
    def n_repeats(self, n_repeats):
        assert n_repeats >= 1, 'Number of repetitions must be greater than 0'
        self._n_repeats = n_repeats

    @property
    def n_folds(self):
        return self._n_folds

    @n_folds.setter
    def n_folds(self, n_folds):
        assert n_folds >= 1, \
            'Number of folds in k-fold splitting must be greater than 1'
        self._n_folds = n_folds


class KFoldSplitter:
    """Split data into training and validation sets using k-fold strategy.

    Arguments
    ---------
    n_folds : int, default: 5
        Number of folds.
    shuffle : bool, default: True
        Whether to shuffle data before splitting.
    random_state : int, default: None (random number)
        Random state/seed.

    Examples
    --------
    >>> import deepchem as dc
    >>> import numpy as np
    >>> n_examples = 100
    >>> n_features = 10
    >>> n_outputs = 2
    >>> X = np.random.normal(loc=0, scale=1, size=(n_examples, n_features))
    >>> y = np.random.randint(low=0, high=2, size=(n_examples, n_outputs))
    >>> data_set = dc.data.DiskDataset.from_numpy(X, y, verbose=False)
    >>> splitter = KFoldSplitter(n_folds=3, random_state=42)
    >>> splitter
    KFoldSplitter(
        n_folds=3,
        shuffle=True,
        random_state=42
    )
    >>> for train_data, valid_data in splitter.split(data_set):
    ...     print(len(train_data), len(valid_data))
    67 33
    67 33
    67 33
    >>> splitter.n_folds = 0
    Traceback (most recent call last):
    ...
    AssertionError: Number of folds in k-fold splitting must be greater than 1
    or equal to -1 (LOO strategy)
    >>> splitter.n_folds = 2
    >>> splitter.shuffle = False
    >>> train_data, valid_data = next(splitter.split(data_set))
    >>> np.array_equal(train_data.X, data_set.X[50:])
    True
    >>> np.array_equal(valid_data.X, data_set.X[:50])
    True
    >>> splitter.shuffle = True
    >>> train_data, valid_data = next(splitter.split(data_set))
    >>> np.array_equal(train_data.X, data_set.X[50:])
    False
    """
    def __init__(self,
                 n_folds=5,
                 shuffle=True,
                 random_state=None):
        self.n_folds = n_folds
        self.shuffle = shuffle
        self.random_state = random_state

    def __repr__(self):
        return (
            '{0.__class__.__name__}(\n'
            '    n_folds={0.n_folds},\n'
            '    shuffle={0.shuffle},\n'
            '    random_state={0.random_state}\n)'
            .format(self)
        )

    @property
    def n_folds(self):
        return self._n_folds

    @n_folds.setter
    def n_folds(self, n_folds):
        assert n_folds > 1 or n_folds == -1, \
            'Number of folds in k-fold splitting must be greater than 1\n' \
            'or equal to -1 (LOO strategy)'
        self._n_folds = n_folds

    @property
    def shuffle(self):
        return self._shuffle

    @shuffle.setter
    def shuffle(self, shuffle):
        assert isinstance(shuffle, bool), '`shuffle` must be boolean'
        self._shuffle = shuffle

    def split(self, data_set):
        """Generate training and validation sets.

        Parameters
        ----------
        data_set : deepchem.data.DiskDataset
            Input objects and output values.

        Yields
        -------
        sets : generator of tuples
            data_train : deepchem.data.DiskDataset
                Training data set.
            data_valid : deepchem.data.DiskDataset
                Validation data set.
        """
        for index in self._get_indices(data_set):
            x_train = np.delete(data_set.X, index, axis=0)
            y_train = np.delete(data_set.y, index, axis=0)
            data_train = dc.data.DiskDataset.from_numpy(
                x_train,
                y_train,
                verbose=False,
            )

            x_valid = data_set.X[index]
            y_valid = data_set.y[index]
            data_valid = dc.data.DiskDataset.from_numpy(
                x_valid,
                y_valid,
                verbose=False,
            )

            yield data_train, data_valid

    def _get_indices(self, data_set):
        """Get indices of validation data so that we can exclude
        corresponding data to get train-valid sets.

        Parameters
        ----------
        data_set : deepchem.data.DiskDataset
            Input objects and output values.

        Returns
        -------
        indices : generator
            Validation data indices.
        """
        if self.n_folds == -1:
            self.n_folds = len(data_set) - 1

        rng = check_random_state(self.random_state)

        idx = np.arange(len(data_set))
        if self.shuffle:
            rng.shuffle(idx)

        n_items = int(len(data_set) / self.n_folds)
        for k_fold in range(self.n_folds):
            current_fold_idx = int(k_fold * n_items)

            yield idx[current_fold_idx:current_fold_idx + n_items]


class LeaveOneOutSplitter(KFoldSplitter):
    """Split data into training and validation sets
    using leave-one-out strategy.
    """
    def __init__(self):
        super().__init__(-1, False)
