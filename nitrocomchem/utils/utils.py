"""
Utility functions and classes.
"""

import collections
import functools

import deepchem as dc
import numpy as np

from sklearn.metrics import make_scorer, mean_squared_error


class AttributeDelegator:
    """The functor that delegates `attribute_name`s methods in `method_names`
    to a specified object (e.g. class).

    Parameters
    ----------
    attribute_name : str
        An attribute of an object (e.g. class).
    method_names : iterable
        Method names of an attribute `attribute_name`.
    """

    def __new__(cls, attribute_name, method_names):
        assert isinstance(attribute_name, str), \
            'Attribute name {0!r} must be str, not {1}.'.format(
                attribute_name, type(attribute_name))
        assert isinstance(method_names, collections.abc.Iterable), \
            'Method space {0!r} must be iterable'.format(method_names)

        cls.NEW_METHOD_SOURCE_CODE = (
            'lambda self, *args, **kwargs: '
            'self.{attribute_name}.{method_name}(*args, **kwargs)'
        )

        return super().__new__(cls)

    def __init__(self, attribute_name, method_names):
        if attribute_name.startswith('__'):
            attribute_name = '_' + self.__class__.__name__ + attribute_name

        self.attribute_name = attribute_name
        self.method_names = method_names

    def __call__(self, cls):
        for method_name in self.method_names:
            setattr(
                cls,
                method_name,
                eval(
                    self.NEW_METHOD_SOURCE_CODE.format(
                        attribute_name=self.attribute_name,
                        method_name=method_name
                    )
                )
            )

        return cls

    delegate = __call__


def rmse_score(y_true, y_pred, multioutput='var'):
    """RMSE regression score function.

    Parameters
    ----------
    y_true : array-like, shape = (n_samples) or (n_samples, n_tasks)
        Correct target values.
    y_pred : array-like, shape = (n_samples) or (n_samples, n_tasks)
        Estimated target values.
    multioutput : str in ['mean', 'var', 'std', 'range', 'raw'], default: 'var'
        Weight averaging of multiple outputs.

    Returns
    -------
    rmse : float or array-like of floats of shape = (n_tasks)
        Single-output or weight averaged multi-output RMSE or ndarray of
        multiple scores.
    """
    mse_multioutput = None if y_true.ndim == 1 else 'raw_values'

    score = np.sqrt(
        mean_squared_error(y_true, y_pred, multioutput=mse_multioutput))

    if y_true.ndim == 1 or multioutput == 'raw':
        return score
    else:
        avg_weights = None

        if multioutput == 'mean':
            avg_weights = np.mean(y_true, axis=0)
        elif multioutput == 'var':
            avg_weights = np.var(y_true, axis=0)
        elif multioutput == 'std':
            avg_weights = np.std(y_true, axis=0)
        elif avg_weights == 'range':
            avg_weights = np.max(y_true, axis=0) - np.min(y_true, axis=0)

        return np.average(score, axis=0, weights=avg_weights)


root_mean_squared_error = make_scorer(rmse_score, greater_is_better=False)


def deepchem_rmse(tasks, weights=None):
    """Weighted RMSE scorer corresponding to deepchem metrics.

    Parameters
    ----------
    tasks : array-like, shape = (n_samples, n_tasks)
        Task observations.
    weights : array-like, shape = n_tasks, default: None
        Task weights. If None, calculate the variances of tasks.

    Returns
    -------
    metric : deepchem.metrics.Metric
        Deepchem compatible metric.
    """
    weights = weights or np.nanvar(tasks, axis=0)

    task_averager = functools.partial(np.average, axis=0, weights=weights)
    task_averager.__name__ = 'weighted_mean'

    rmse = dc.metrics.Metric(
        rmse_score,
        task_averager=task_averager,
        mode='regression',
        verbose=False,
    )
    return rmse


def load_csv_data(filenames,
                  tasks,
                  featurizer,
                  transformers=None):
    """Load, featurize and transform the data set.

    Parameters
    ----------
    filenames : iterable[str or path-like]
        Paths to data sets.
    tasks : iterable[str]
        Task names.
    featurizer : deepchem.feat.Featurizer
        Featurizer instance.
    transformers : iterable[deepchem.trans.Transformer]
        Set of transformers.

    Returns
    -------
    data_set : deepchem.data.DiskDataset
        Featurized and transformed data set.
    """

    csv_loader = dc.data.CSVLoader(
        tasks=tasks,
        smiles_field='smiles',
        featurizer=featurizer,
        verbose=False,
    )
    data_set = csv_loader.featurize(filenames, shard_size=90)

    if transformers is not None:
        for transformer in transformers:
            transformer.dataset = data_set
            data_set = transformer.transform(data_set)

    return data_set
