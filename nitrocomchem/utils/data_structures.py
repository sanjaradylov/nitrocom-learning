"""
More useful data structures.
"""

import bisect
import collections

# from .utils import AttributeDelegator


# @AttributeDelegator(
#     '_data',
#     [
#         '__eq__',
#         '__getitem__',
#         '__iter__',
#         '__len__',
#         '__reversed__',
#         '__repr__',
#         'pop',
#     ]
# )
class SortedList(collections.abc.Sequence):
    """Creates a sorted sequence ordered by a specified `key` function.

    Parameters
    ----------
    sequence : list-like or SortedList, default: None
        Initial values.
    key_function : callable, default: identity (< ordering)
        Key function that customizes the sort order.

    Methods
    -------
    All abstract and mixin methods of collections.abc.Sequence ABC.

    index(item: Any, start: int = 0, end: int = -1) -> int
        Find the position of the item in the list.
    count(item: Any) -> int
        Count the number of occurrences of the item in the list.
    insert(item: Any)
        Insert the item into the list.
    remove(item: Any)
        Remove the item from the list.
    extend(sequence: collections.abc.Sequence)
        Update the list by adding the items from sequence.

    Properties
    ----------
    key_function: callable, default: identity (< ordering)
        Key function.
    """
    __slots__ = (
        '__key_function',
        '_data',
    )

    def __init__(self, sequence=None, key_function=None):
        sequence, key_function = self._validate_parameters(
            sequence, key_function)

        self.__key_function = key_function

        if isinstance(sequence, SortedList):
            self._data = sequence._data.copy()
        else:
            self._data = sorted(sequence, key=self.__key_function)

    @property
    def key_function(self):
        return self.__key_function

    def __repr__(self):
        return repr(self._data)

    def __getitem__(self, index):
        return self._data.__getitem__(index)

    def __len__(self):
        return len(self._data)

    def __iter__(self):
        return iter(self._data)

    def __reversed__(self):
        return reversed(self._data)

    def __eq__(self, other):
        return self._data == other

    def __bool__(self):
        return bool(self._data)

    def __contains__(self, item):
        """Check if `item` is in a sorted list.

        Notes
        -----
        Complexity: O(lgn)
        """
        index = bisect.bisect_left(self._data, self.__key_function(item))

        return self.__item_in_list(index, item)

    def __add__(self, other):
        new_sequence = self.copy()
        new_sequence.extend(other)

        return new_sequence

    def __iadd__(self, other):
        self.extend(other)

    def index(self, item, start=0, end=-1):
        """Find the position of `item` in the sorted list.

        Notes
        -----
        Complexity: O(lgn)
        """
        if end == -1:
            end = len(self._data) - 1

        index = bisect.bisect_left(self._data, self.__key_function(item),
                                   start, end)
        if self.__item_in_list(index, item):
            return index
        else:
            raise ValueError(
                '{0}.index(x): {1} not in list'.format(
                    self.__class__.__name__, item)
            )

    def count(self, item):
        """Count the number of occurrences of `item` in the sorted list.

        Notes
        -----
        Complexity: O(lgn)
        """
        comparable_item = self.__key_function(item)

        left_index = bisect.bisect_left(self._data, comparable_item)
        right_index = bisect.bisect_right(self._data, comparable_item)

        return right_index - left_index

    def insert(self, item):
        """Insert `item` and keep the order.

        Notes
        -----
        Complexity: O(lgn)
        """
        index = bisect.bisect_left(self._data, self.__key_function(item))

        if index == len(self._data):
            self._data.append(item)
        else:
            self._data.insert(index, item)

    def remove(self, item):
        """Remove `item` and keep the order.

        Notes
        -----
        Complexity: O(lgn)
        """
        index = bisect.bisect_left(self._data, self.__key_function(item))
        if self.__item_in_list(index, item):
            del self._data[index]
        else:
            raise ValueError(
                '{0}.remove(x): {1} not in list'.format(
                    self.__class__.__name__, item)
            )

    def pop(self):
        return self._data.pop()

    def extend(self, sequence):
        """Update a sorted list adding items from `sequence` and keeping the
        order.

        Parameters
        ----------
        sequence : collections.abc.Sequence registered
            An arbitrary sequence.

        Notes
        -----
        Complexity: O(m * lg(n+m)), where m = len(sequence), n = len(self).
        """
        for item in sequence:
            self.insert(item)

    def copy(self):
        return self.__class__(self, self.__key_function)

    __copy__ = copy

    def clear(self):
        self._data = []

    @classmethod
    def _validate_parameters(cls, sequence, key_function):
        sequence = sequence or []
        if not isinstance(sequence, collections.abc.Iterable):
            raise ValueError('Sequence must be iterable.')

        key_function = key_function or (lambda x: x)
        if not hasattr(key_function, '__call__'):
            raise ValueError(
                'Key function {0!r} must be callable'.format(key_function)
            )

        return sequence, key_function

    def __item_in_list(self, index, item):
        return index < len(self._data) and self._data[index] == item


class SortedDict(dict):
    """Creates a sorted dictionary with the keys ordered by a specified
    key function.

    Parameters
    ----------
    dictionary : dict-like, default: None
        Initial key-value pairs.
    key_function : callable, default: None (< ordering)
        Key function.
    kwargs : key-values, optional
        Additional key-value pairs.

    Methods
    -------
    All methods of dict.

    Properties
    ----------
    key_function: callable, default: identity (< ordering)
        Key function.

    Notes
    -----
    New sorted dict can be initialized only by MutableMapping ABC instance
    (e.g. dict, collections.OrderedDict, collections.ChainMap, SortedDict).
    super() might be expensive to call. Maybe use cache references? What about
    inheriting collections.UserDict? Is it space-consuming?
    """

    def __init__(self, dictionary=None, key_function=None, **kwargs):
        dictionary, key_function = self._validate_parameters(
            dictionary, key_function)

        if kwargs:
            dictionary.update(kwargs)

        if isinstance(dictionary, SortedDict):
            self.__key_function = dictionary.key_function
            self._keys = dictionary._keys.copy()
        else:
            self.__key_function = key_function
            self._keys = SortedList(dictionary.keys(), key_function)

        super().__init__(dictionary)

    @property
    def key_function(self):
        return self.__key_function

    def __repr__(self):
        return '{{{sorted_dict}}}'.format(sorted_dict=', '.join(
            '{0!r}: {1!r}'.format(key, value)
            for key, value in self.items()
        ))

    def __iter__(self):
        return iter(self._keys)

    def __setitem__(self, key, value):
        if key not in self:
            self._keys.insert(key)
        super().__setitem__(key, value)

    def __delitem__(self, key):
        if key in self:
            self._keys.remove(key)
            super().__delattr__(key)
        else:
            raise KeyError(key)

    keys = __iter__

    def values(self):
        return (self[key] for key in self._keys)

    def items(self):
        return ((key, self[key]) for key in self._keys)

    def update(self, dictionary=None, **kwargs):
        if not isinstance(dictionary, collections.abc.MutableMapping):
            raise ValueError(
                'Dictionary must be registered as MutableMapping ABC.'
            )

        if dictionary is not None:
            super().update(dictionary)
        if kwargs:
            super().update(kwargs)

        self._keys = SortedList(super().keys(), self.__key_function)

    def pop(self, key, *args):
        if key not in self:
            if not args:
                raise KeyError(key)
            return args[0]

        self._keys.remove(key)

        return super().pop(key, args)

    def popitem(self):
        key, value = super().popitem()
        self._keys.remove(key)

        return key, value

    def setdefault(self, key, default=None):
        if key not in self:
            self._keys.insert(key)
        return super().setdefault(key, default)

    def clear(self):
        super().clear()
        self._keys.clear()

    def copy(self):
        return self.__class__(self)

    __copy__ = copy

    @classmethod
    def fromkeys(cls, keys, value=None, key_function=None):
        return cls(
            dictionary={key: value for key in keys},
            key_function=key_function,
        )

    @classmethod
    def _validate_parameters(cls, dictionary, key_function):
        dictionary = dictionary or {}
        if not isinstance(dictionary, collections.abc.MutableMapping):
            raise ValueError(
                'Dictionary must be registered as MutableMapping ABC.'
            )

        key_function = key_function or (lambda x: x)
        if not hasattr(key_function, '__call__'):
            raise ValueError(
                'Key function {0!r} must be callable'.format(key_function)
            )

        return dictionary, key_function
