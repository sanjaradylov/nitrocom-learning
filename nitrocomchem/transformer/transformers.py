"""
Utilities for feature and target transformations.

Classes
-------
YeoJohnsonTransformer
    Yeo-Johnson power transformations (scikit-learn API).

Notes
-----
Modern versions of scikit-learn provide more flexible transformer classes
including power and quantile transformers. Deepchem constraints us to use
version 0.19.

References
----------
scikit-learn.org/stable/modules/classes.html#module-sklearn.preprocessing
"""

import numpy as np

from scipy import optimize

from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.preprocessing import StandardScaler
from sklearn.utils import check_array
from sklearn.utils.validation import check_is_fitted, FLOAT_DTYPES


class YeoJohnsonTransformer(BaseEstimator, TransformerMixin):
    """Apply a power transform featurewise to make data more Gaussian-like.
    Power transforms are a family of parametric, monotonic transformations
    that are applied to make data more Gaussian-like. This is useful for
    modeling issues related to heteroscedasticity (non-constant variance),
    or other situations where normality is desired.
    Supports Yeo-Johnson transform. The optimal parameter for stabilizing
    variance and minimizing skewness is estimated through maximum likelihood.
    Supports both positive and negative data.
    By default, zero-mean, unit-variance normalization is applied to the
    transformed data.

    Parameters
    ----------
    standardize : boolean, default=True
        Set to True to apply zero-mean, unit-variance normalization to the
        transformed output.

    Attributes
    ----------
    lambdas_ : array of float, shape (n_features,)
        The parameters of the power transformation for the selected features.

    Examples
    --------
    >>> import numpy as np
    >>> pt = YeoJohnsonTransformer()
    >>> data = [[1, 2], [3, 2], [4, 5]]
    >>> print(pt.fit(data))
    YeoJohnsonTransformer(standardize=True)
    >>> print(pt.lambdas_)
    [ 1.386... -3.100...]
    >>> print(pt.transform(data))
    [[-1.316... -0.707...]
     [ 0.209... -0.707...]
     [ 1.106...  1.414...]]

    Notes
    -----
    NaNs are treated as missing values: disregarded in ``fit``, and maintained
    in ``transform``.

    References
    ----------
    .. [1] I.K. Yeo and R.A. Johnson, "A new family of power transformations to
           improve normality or symmetry." Biometrika, 87(4), pp.954-959,
           (2000).
    .. [2] G.E.P. Box and D.R. Cox, "An Analysis of Transformations", Journal
           of the Royal Statistical Society B, 26, 211-252 (1964).
    """

    def __init__(self, standardize=True):
        self.standardize = standardize

    def fit(self, X, y=None):
        """Estimate the optimal parameter lambda for each feature.
        The optimal lambda parameter for minimizing skewness is estimated on
        each feature independently using maximum likelihood.

        Parameters
        ----------
        X : array-like, shape (n_samples, n_features)
            The data used to estimate the optimal transformation parameters.
        y : Ignored

        Returns
        -------
        self : object
        """
        self._fit(X, y=y, force_transform=False)
        return self

    def fit_transform(self, X, y=None, **fit_params):
        return self._fit(X, y, force_transform=True)

    def _fit(self, X, y=None, force_transform=False):
        X = self._check_input(X)

        with np.errstate(invalid='ignore'):  # hide NaN warnings
            self.lambdas_ = np.array([
                self._yeo_johnson_optimize(col) for col in X.T
            ])

        if self.standardize or force_transform:
            for i, lmbda in enumerate(self.lambdas_):
                with np.errstate(invalid='ignore'):  # hide NaN warnings
                    X[:, i] = self._yeo_johnson_transform(X[:, i], lmbda)

        if self.standardize:
            self._scaler = StandardScaler(copy=False)
            if force_transform:
                X = self._scaler.fit_transform(X)
            else:
                self._scaler.fit(X)

        return X

    def transform(self, X):
        """Apply the power transform to each feature using the fitted lambdas.

        Parameters
        ----------
        X : array-like, shape (n_samples, n_features)
            The data to be transformed using a power transformation.

        Returns
        -------
        X_trans : array-like, shape (n_samples, n_features)
            The transformed data.
        """
        check_is_fitted(self, 'lambdas_')
        X = self._check_input(X, check_shape=True)

        for i, lmbda in enumerate(self.lambdas_):
            with np.errstate(invalid='ignore'):  # hide NaN warnings
                X[:, i] = self._yeo_johnson_transform(X[:, i], lmbda)

        if self.standardize:
            X = self._scaler.transform(X)

        return X

    def inverse_transform(self, X):
        """Apply the inverse power transformation using the fitted lambdas.
        The inverse of the Box-Cox transformation is given by::
            if lambda == 0:
                X = exp(X_trans)
            else:
                X = (X_trans * lambda + 1) ** (1 / lambda)
        The inverse of the Yeo-Johnson transformation is given by::
            if X >= 0 and lambda == 0:
                X = exp(X_trans) - 1
            elif X >= 0 and lambda != 0:
                X = (X_trans * lambda + 1) ** (1 / lambda) - 1
            elif X < 0 and lambda != 2:
                X = 1 - (-(2 - lambda) * X_trans + 1) ** (1 / (2 - lambda))
            elif X < 0 and lambda == 2:
                X = 1 - exp(-X_trans)

        Parameters
        ----------
        X : array-like, shape (n_samples, n_features)
            The transformed data.

        Returns
        -------
        X : array-like, shape (n_samples, n_features)
            The original data
        """
        check_is_fitted(self, 'lambdas_')
        X = self._check_input(X, check_shape=True)

        if self.standardize:
            X = self._scaler.inverse_transform(X)

        for i, lmbda in enumerate(self.lambdas_):
            with np.errstate(invalid='ignore'):  # hide NaN warnings
                X[:, i] = self._yeo_johnson_inverse_transform(X[:, i], lmbda)

        return X

    @staticmethod
    def _yeo_johnson_inverse_transform(x, lmbda):
        """Return inverse-transformed input x following Yeo-Johnson inverse
        transform with parameter lambda.
        """
        x_inv = np.zeros_like(x)
        pos = x >= 0

        # when x >= 0
        if abs(lmbda) < np.spacing(1.):
            x_inv[pos] = np.exp(x[pos]) - 1
        else:  # lmbda != 0
            x_inv[pos] = np.power(x[pos] * lmbda + 1, 1 / lmbda) - 1

        # when x < 0
        if abs(lmbda - 2) > np.spacing(1.):
            x_inv[~pos] = 1 - np.power(-(2 - lmbda) * x[~pos] + 1,
                                       1 / (2 - lmbda))
        else:  # lmbda == 2
            x_inv[~pos] = 1 - np.exp(-x[~pos])

        return x_inv

    @staticmethod
    def _yeo_johnson_transform(x, lmbda):
        """Return transformed input x following Yeo-Johnson transform with
        parameter lambda.
        """

        out = np.zeros_like(x)
        pos = x >= 0  # binary mask

        # when x >= 0
        if abs(lmbda) < np.spacing(1.):
            out[pos] = np.log1p(x[pos])
        else:  # lmbda != 0
            out[pos] = (np.power(x[pos] + 1, lmbda) - 1) / lmbda

        # when x < 0
        if abs(lmbda - 2) > np.spacing(1.):
            out[~pos] = -(np.power(-x[~pos] + 1, 2 - lmbda) - 1) / (2 - lmbda)
        else:  # lmbda == 2
            out[~pos] = -np.log1p(-x[~pos])

        return out

    def _yeo_johnson_optimize(self, x):
        """Find and return optimal lambda parameter of the Yeo-Johnson
        transform by MLE, for observed data x.
        Like for Box-Cox, MLE is done via the brent optimizer.
        """

        def _neg_log_likelihood(lmbda):
            """Return the negative log likelihood of the observed data x as a
            function of lambda."""
            x_trans = self._yeo_johnson_transform(x, lmbda)
            n_samples = x.shape[0]

            loglike = -n_samples / 2 * np.log(x_trans.var())
            loglike += (lmbda - 1) * (np.sign(x) * np.log1p(np.abs(x))).sum()

            return -loglike

        # the computation of lambda is influenced by NaNs so we need to
        # get rid of them
        x = x[~np.isnan(x)]
        # choosing bracket -2, 2 like for boxcox
        return optimize.brent(_neg_log_likelihood, brack=(-2, 2))

    def _check_input(self, X, check_shape=False):
        """Validate the input before fit and transform.

        Parameters
        ----------
        X : array-like, shape (n_samples, n_features)
        check_shape : bool
            If True, check that n_features matches the length of self.lambdas_

        Returns
        -------
        X_t : array-like, shape (n_samples, n_features)
            The same data (maybe transformed into numpy.array)
        """
        X = check_array(X, ensure_2d=True, dtype=FLOAT_DTYPES, copy=True,
                        force_all_finite='allow-nan')

        with np.warnings.catch_warnings():
            np.warnings.filterwarnings(
                'ignore', r'All-NaN (slice|axis) encountered'
            )

        if check_shape and not X.shape[1] == len(self.lambdas_):
            raise ValueError(
                'Input data has a different number of features '
                'than fitting data. Should have {n}, data has {m}'
                .format(n=len(self.lambdas_), m=X.shape[1])
            )

        return X
