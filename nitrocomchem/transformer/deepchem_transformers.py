"""
Utilities for feature and target transformations.

Classes
-------
DeepChemTransformer
    Feature and/or target transformations (deepchem API supporting
    scikit-learn transformers).
"""

import deepchem as dc

from .transformers import YeoJohnsonTransformer


class DeepChemTransformer(dc.trans.transformers.Transformer):
    """Apply transformations based on specified scikit-learn API transformers.
    Follows Deepchem API.

    Parameters
    ----------
    transformer : scikit-learn transformer API, default: YeoJohnsonTransformer
        Transformations to apply.
    tasks : list-like, default None
        When transforming multiple outputs, specify task mask
        (None for every task).
    """
    def __init__(self,
                 transform_X=False,
                 transform_y=False,
                 transform_w=False,
                 dataset=None,
                 transformer=None,
                 tasks=None):
        self.transformer = transformer or YeoJohnsonTransformer()
        self.tasks = tasks

        super().__init__(transform_X, transform_y, transform_w, dataset)

    def transform_array(self, X, y, w):
        if self.transform_X:
            X = self.transformer.fit_transform(X)
        if self.transform_y:
            y[:, self.tasks] = self.transformer.fit_transform(y[:, self.tasks])
        if self.transform_w:
            w = self.transformer.fit_transform(w)

        return X, y, w

    def untransform(self, z):
        # The number of tasks might be equal to the number of features.
        # If this is the case, the condition is ambiguous.
        # Nothing to do with it; it is Deepchem Transformer API.
        if z.shape[1] == len(self.tasks):
            z[:, self.tasks] = self.transformer.inverse_transform(
                z[:, self.tasks])
        else:
            z = self.transformer.inverse_transform(z)

        return z
