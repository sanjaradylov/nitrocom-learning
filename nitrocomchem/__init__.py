"""
Machine learning module for analysis of nitro compounds.
========================================================

Based on scikit-learn and deepchem packages. Mostly uses existing tools for
analysis (see for example run.py) but provides new features when there are no
valid working solutions.
"""

from . import (
    featurizer,
    model,
    splitter,
    transformer,
    utils,
)
