"""
Multitask regression models from Deepchem.

Functions
---------
load_deepchem_model
    Load one of the pre-tuned deepchem models.
"""

import deepchem as dc
import tensorflow as tf


def load_deepchem_model(tasks,
                        data,
                        model_name='graph_conv',
                        fit=True,
                        n_features=None):
    """Load a pre-tuned deepchem model.

    Parameters
    ----------
    tasks : list-like
        Regression tasks.
    data : deepchem.data.DiskDataset
        Data set to learn from.
    model_name : str, default: 'graph_conv'
        See model_map dictionary.
    fit : bool, default: True
        Whether to fit the model on data.
    n_features : int, default: None
        Number of features (only for MLPs).

    Returns
    -------
    model : deepchem.models.TensorGraph
        Deepchem model.
    """
    assert model_name in [
        'bypass',
        'graph_conv',
        'mlp',
        'mpnn',
        'pmlp',
        'weave',
    ], 'Unsupported model {0!r}.'.format(model_name)

    model_map = {
        'bypass': dc.models.RobustMultitaskRegressor(
            len(tasks),
            n_features or 2048,
            layer_sizes=[n_features],
            weight_init_stddevs=0.02,
            bias_init_consts=1.0,
            weight_decay_penalty=0.0,
            weight_decay_penalty_type='l2',
            dropouts=0.0,
            activation_fns=tf.nn.relu,
            bypass_layer_sizes=[n_features],
            bypass_weight_init_stddevs=[.02],
            bypass_bias_init_consts=[1.],
            bypass_dropouts=[.5],
        ),
        'graph_conv': dc.models.GraphConvModel(
            n_tasks=len(tasks),
            graph_conv_layers=[64, 64],
            dense_layer_size=128,
            dropout=0.0,
            mode='regression',
            number_atom_features=75,
            uncertainty=False,
        ),
        'mlp': dc.models.MultitaskRegressor(
            n_tasks=len(tasks),
            n_features=n_features or 2048,
            layer_sizes=[2048],
            weight_init_stddevs=0.02,
            bias_init_consts=1.0,
            weight_decay_penalty=0.0,
            weight_decay_penalty_type='l2',
            dropouts=0.5,
            activation_fns=tf.nn.relu,
            uncertainty=False,
        ),
        'mpnn': dc.models.MPNNModel(
            len(tasks),
            n_atom_feat=75,
            n_pair_feat=14,
            T=3,
            M=5,
            batch_size=2,
            learning_rate=0.0001,
            use_queue=False,
            mode='regression',

            random_seed=0,
        ),
        'pmlp': dc.models.ProgressiveMultitaskRegressor(
            len(tasks),
            n_features or 2048,
            layer_sizes=[1000],
            weight_decay_penalty=0.001,
            weight_decay_penalty_type='l1',
            dropouts=0.4,
            activation_fns=tf.nn.relu,
            n_outputs=1,
        ),
        'weave': dc.models.WeaveModel(
            len(tasks),
            n_atom_feat=75,
            n_pair_feat=14,
            n_hidden=50,
            n_graph_feat=128,
            mode='regression',

            random_seed=0,
        ),
    }

    model = model_map[model_name]

    if fit:
        model.fit(
            dataset=data,
            nb_epoch=50,
            batch_size=2,
            # batchnorm=False,
            # learning_rate=0.0001,
            # momentum=0.8,
            # decay=1e-5,
            # nesterov=True,
            # checkpoint_interval=3,
            # droupout=0.2,
        )

    return model
