"""
Deepchem featurizers.

Classes
-------
CoulombMatrixFeaturizer
    Generate coulomb matrices.
"""

import deepchem as dc
import numpy as np

from deepchem.utils import pad_array
from rdkit import Chem
from rdkit.Chem import AllChem

from .utils import MaxAtomNumberError


class CoulombMatrixFeaturizer(dc.feat.CoulombMatrix):
    """Calculate Coulomb matrices for molecules.

    Notes
    -----
    Deepchem implementation doesn't work, so I had to re-implement it.
    See deepchem.feat.CoulombMatrix.
    """
    def coulomb_matrix(self, molecule):
        """Generate Coulomb matrices for each conformer of the given molecule.

        Parameters
        ----------
        molecule : rdkit.Chem.Mol
            Molecule instance.

        Returns
        -------
        features : list of numpy ndarrays
            Coulomb matrices for every conformer.
        """
        molecule_with_conformations = Chem.AddHs(molecule)
        AllChem.EmbedMolecule(molecule_with_conformations)

        n_atoms = molecule_with_conformations.GetNumAtoms()
        if self.max_atoms < n_atoms:
            raise MaxAtomNumberError(
                'The maximum number of atoms ("{0}") is less than '
                'the number of atoms of molecule {1!r} ("{2}").'
                .format(self.max_atoms, molecule, n_atoms)
            )

        nuclear_charges = [
            atom.GetAtomicNum()
            for atom in molecule_with_conformations.GetAtoms()
        ]

        features = []
        for conformation in molecule_with_conformations.GetConformers():
            interatomic_distances = self.get_interatomic_distances(
                conformation)

            matrix = np.zeros((n_atoms, n_atoms))
            for i in range(n_atoms):
                for j in range(i, n_atoms):
                    if i == j:
                        matrix[i, j] = 0.5 * nuclear_charges[i]**2.4
                    else:
                        matrix[i, j] = (
                            nuclear_charges[i] * nuclear_charges[j]
                            / interatomic_distances[i, j]
                        )
                        matrix[j, i] = matrix[i, j]

            if self.randomize:
                for random_matrix in self.randomize_coulomb_matrix(matrix):
                    random_matrix = pad_array(random_matrix, self.max_atoms)
                    features.append(random_matrix)
            else:
                matrix = pad_array(matrix, self.max_atoms)
                features.append(matrix)

            return features
