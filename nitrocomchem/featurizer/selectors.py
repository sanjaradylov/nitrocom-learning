"""
Feature selection.
"""

import functools

import numpy as np

from sklearn.base import BaseEstimator
from sklearn.linear_model import LinearRegression
from sklearn.utils import check_array
from sklearn.feature_selection.base import SelectorMixin
from sklearn.utils.validation import check_is_fitted


class VIFSelector(BaseEstimator, SelectorMixin):
    """Variance Inflation Factor (VIF) is a measure for multicollinearity.

    Calculate VIF for every feature of data and drop those having VIFs
    greater than given `threshold`.

    Parameters
    ----------
    threshold : float, default: 5.0
        Threshold indicating high collinearity of a feature with the other
        features. Common recommendation is >= 5.0.
    solver_kwargs : dict-like, default: None
        Additional keyword arguments for LinearRegression.

    Attributes
    ----------
    mask_ : numpy ndarray, shape = n_features
        Selected features mask.

    Examples
    --------
    >>> import numpy as np
    >>> X = np.array([
    ...      [1, 3,  8,   -5,  -9],
    ...      [2, 6,  -10, 8,   15],
    ...      [3, 9,  3,   -15, -30],
    ...      [4, 12, 19,  2,   4],
    ...      [5, 15, -7,  8,   16],
    ...      [6, 18, -23, 7,   14]])
    >>> vifs = VIFSelector()
    >>> X_t = vifs.fit_transform(X)
    >>> vifs.mask_.tolist()
    [False, True, True, False, True]
    """
    def __init__(self, threshold=5.0, solver_kwargs=None):
        self.threshold = threshold
        self.solver_kwargs = solver_kwargs or {}

    def fit(self, X, y=None):
        """Fit linear regressions for every feature and score VIFs until all
        scores are less than given threshold.

        Parameters
        ----------
        X : array-like, shape = (n_samples, n_features)
            The feature space.

        Returns
        -------
        self : VIFSelector
        """
        X = check_array(X, accept_sparse=False)
        n_features = X.shape[1]
        features = np.arange(n_features)

        # Boolean mask of chosen features
        self.mask_ = np.ones_like(features, dtype=bool)

        lin_reg = LinearRegression(**self.solver_kwargs)

        # At least 1 feature must be kept
        for iteration in range(n_features - 1):

            # Compute VIFs for currently available features
            scores = []
            for feature in features[self.mask_]:
                # Temporarily off `feature` as it is dependent variable now
                # (little trick to prevent from assigning new masks every time)
                self.mask_[feature] = False

                # Fit and score
                lin_reg.fit(X[:, self.mask_], X[:, feature])
                r2 = lin_reg.score(X[:, self.mask_], X[:, feature])
                vif = 1 / (1 - r2 + 1e-7)
                scores.append(vif)

                # Turn back
                self.mask_[feature] = True

            # Store scores in array of shape n_features, where 0. means that
            # the corresponding feature was rejected for being collinear
            all_scores = np.zeros_like(features, dtype=np.float64)
            all_scores[features[self.mask_]] = scores

            # Get the feature with maximum VIF score
            max_vif_feature = all_scores.argmax()
            if all_scores[max_vif_feature] > self.threshold:
                # Do not use this feature as it is highly collinear with others
                self.mask_[max_vif_feature] = False
            else:
                # There is no multicollinearity in the feature space
                # features[self.mask_], so keep it
                break

        return self

    def _get_support_mask(self):
        check_is_fitted(self, 'mask_')
        return self.mask_


class NonemptyFeatureSelector(BaseEstimator, SelectorMixin):
    """Remove feature if the amount of `empty_value`s doesn't meet some
    `threshold`.

    Parameters
    ----------
    empty_value : any type that is supported by numpy
        The value considered empty and non-informative.
    threshold : float or int
        If int, the number `empty_value`s allowable to consider a feature
        informative.
        If float, the percentage of `empty_value`s.

    Attributes
    ----------
    empty_value_counts_ : numpy ndarray, shape = n_features
        The number of empty values for every feature.
    """
    def __init__(self, empty_value=0, threshold=0.0):
        self.empty_value = empty_value
        self.threshold = threshold

    @staticmethod
    def count_empty_values(x, empty_value):
        x = check_array(x, ('csr', 'csc'), ensure_2d=False)

        return np.count_nonzero(x == empty_value)

    def fit(self, X, y=None):
        X = check_array(X, ('csr', 'csc'))

        count_empty_values = functools.partial(
            self.count_empty_values,
            empty_value=self.empty_value,
        )
        self.empty_value_counts_ = np.apply_along_axis(
            func1d=count_empty_values,
            axis=0,
            arr=X,
        )

        return self

    def _get_support_mask(self):
        check_is_fitted(self, 'empty_value_counts_')
        return self.empty_value_counts_ <= self.threshold
