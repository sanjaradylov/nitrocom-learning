"""
Functions
---------
load_deepchem_featurizers
    Load pre-tuned deepchem featurizers.
"""

import deepchem as dc


class MaxAtomNumberError(ValueError):
    """Raise an exception when maximum number of atoms specified is less than
    the dimension of generated feature space.
    """


def load_deepchem_featurizers(featurizer_name='weave', n_features=None):
    """Load pre-tuned deepchem featurizers.

    Parameters
    ----------
    featurizer_name : str, default: 'weave'
        See featurizer_map.
    n_features : int, default: None
        Number of features (only for ECFP).

    Returns
    -------
    featurizer : deepchem.feat.Featurizer
        Featurizer object.
    """
    assert featurizer_name in [
        'graph_conv',
        'weave',
        'mpnn',
        'ecfp',
        'mlp',
        'pmlp',
        'bypass',
    ], 'Unsupported featurizer {0!r}.'.format(featurizer_name)

    graph_conv = dc.feat.ConvMolFeaturizer(
        master_atom=False,
        use_chirality=False,
    )
    circular_fingerprint = dc.feat.CircularFingerprint(
        radius=2,
        size=n_features or 2048,
        chiral=False,
        bonds=True,
        features=False,
        sparse=False,
        smiles=False
    )
    weave = dc.feat.WeaveFeaturizer(
        graph_distance=True,
        explicit_H=False,
        use_chirality=False,
    )

    featurizer_map = {
        'graph_conv': graph_conv,

        'weave': weave,
        'mpnn': weave,

        'ecfp': circular_fingerprint,
        'mlp': circular_fingerprint,
        'pmlp': circular_fingerprint,
        'bypass': circular_fingerprint,
    }
    featurizer = featurizer_map[featurizer_name]

    return featurizer
