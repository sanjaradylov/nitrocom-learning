"""
This script evaluates multi-task Moleculenet models on
nitroaromatic compounds. It is the part of experiments
on nitroaromatic data. See `notebooks` folder for more
analysis on conventional models.
"""

import argparse
import collections
import itertools
import warnings

import numpy as np
import pandas as pd

from nitrocomchem.featurizer import load_deepchem_featurizers
from nitrocomchem.model import load_deepchem_model
from nitrocomchem.splitter import RepeatedKFoldSplitter
from nitrocomchem.transformer import DeepChemTransformer
from nitrocomchem.utils import deepchem_rmse, load_csv_data


warnings.filterwarnings(action='ignore', category=Warning)


FILENAMES = [
    'data/nitrocom_tasks.csv',
]
TASKS = [
    'Log(LD50)',
    'MeltingPoint',
    'BoilingPoint',
]
TASKS_MASK = dict(zip(
    TASKS,
    (
        False,
        True,
        True,
    )
))


def main():
    options = parse_options()

    featurizer = load_deepchem_featurizers(featurizer_name=options.model)

    transformers = []
    if options.transform:
        mask = [TASKS_MASK[task] for task in options.tasks]
        if any(mask):
            transformers.append(
                DeepChemTransformer(transform_y=True, tasks=mask))

    data_set = load_csv_data(
        options.filenames, options.tasks, featurizer, transformers)

    metrics = [
        deepchem_rmse(
            pd.read_csv('data/nitrocom_tasks.csv', usecols=options.tasks)
            .values
        ),
    ]

    splitter = RepeatedKFoldSplitter(
        n_repeats=options.n_repeats,
        n_folds=options.n_folds,
        random_state=0,
    )

    cv_scores = collections.defaultdict(list)

    for train_data, valid_data in splitter.split(data_set):
        for key, data in zip(('train', 'valid'), (train_data, valid_data)):
            model = load_deepchem_model(
                options.tasks,
                train_data,
                model_name=options.model,
            )

            scores = model.evaluate(
                data,
                metrics=metrics,
                transformers=transformers,
                per_task_metrics=True,
            )
            mean_score, task_scores = scores
            cv_scores[key + '_scores'].append(
                mean_score['weighted_mean-rmse_score'])

            for i, task in enumerate(options.tasks):
                cv_scores[key + '_' + task + '_scores'].append(
                    task_scores['weighted_mean-rmse_score'][i])

    for key in 'train', 'valid':
        print('{0} scores:'.format(key.title()))
        print('-------------')
        print(
            'Mean cv score: {0:.3f} (+/-{1:.3f})'.format(
                np.mean(cv_scores[key + '_scores']),
                np.std(cv_scores[key + '_scores'])
            )
        )

        for task in options.tasks:
            print(
                '{0} mean cv score: {1:.3f} (+/-{2:.3f})'.format(
                    task,
                    np.mean(cv_scores[key + '_' + task + '_scores']),
                    np.std(cv_scores[key + '_' + task + '_scores'])
                )
            )

        print()


def parse_options():
    parser = argparse.ArgumentParser(
        description=(
            'Compute single or multi-task cross-validation scores on '
            'nitrocom data using deepchem algorithms.'
        ),
        formatter_class=argparse.RawTextHelpFormatter,
    )

    parser.add_argument(
        '-f', '--filenames',
        type=list,
        default=FILENAMES,
        help='Data files.'
    )
    parser.add_argument(
        '-t', '--tasks',
        nargs='+',
        default=TASKS,
        choices=list(
            itertools.chain(*[
                [' '.join(comb) for comb in itertools.combinations(TASKS, i)]
                for i in range(1, len(TASKS) + 1)
            ])
        ),
        metavar='TASK SET',
        help=(
            'The tasks to evaluate (default: %(default)s).\n'
            'Possible choices are any combination of available tasks.'
        )
    )
    parser.add_argument(
        '-m', '--model',
        choices=['graph_conv', 'weave', 'mlp', 'mpnn', 'bypass'],
        default='weave',
        help='The models to evaluate (default: weave model).'
    )
    parser.add_argument(
        '-r', '--n_repeats',
        type=int,
        default=3,
        choices=list(range(1, 11)),
        metavar='1..10',
        help=(
            'Number of k-fold cross-validation repetitions '
            '(default: %(default)s).'
        )
    )
    parser.add_argument(
        '-k', '--n_folds',
        type=int,
        default=5,
        choices=list(range(2, 11)),
        metavar='2..10',
        help=(
            'Number of folds in k-fold cross-validation '
            '(default: %(default)s).'
        )
    )
    parser.add_argument(
        '-p', '--transform',
        action='store_false',
        help=(
            'Whether to use Yeo-Johnson target transformations on '
            'boiling and melting points (default: yes).'
        )
    )

    return parser.parse_args()


if __name__ == '__main__':
    main()
