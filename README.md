### Source code for paper: Adilov, S. Multi-task regression on nitroaromatic compounds: comparison of traditional and deep learning methods

In this project, we present the original data and source code for the analysis of multi-task nitroaromatic compounds.
The project comprises reproducible benchmarks and a Python package [nitrocomchem](https://bitbucket.org/sanjaradylov/nitrocom-learning/src/master/nitrocomchem/)
built on top of [scikit-learn](https://github.com/scikit-learn/scikit-learn) and [deepchem](https://github.com/deepchem/deepchem).
The main purpose of the package is to introduce additional statistical learning tools for the research addressing the analysis of our data.
Having a constellation of machine learning algorithms, we benchmark the performances of the models we describe in our paper.

---

#### Install dependencies

It is recommended to use [conda environments](https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html).
```
$ conda create -n benchmark python=3.5
$ conda activate benchmark
$ conda install -c deepchem -c rdkit -c conda-forge -c omnia deepchem=2.1.0
```

---

#### Run benchmarks

Deep learning models:
```
$ python run.py [-f FILENAMES -t TASKNAMES -m MODELNAME -n N_REPEATS -k N_FOLDS -p TRANSFORMED]
```
Multi-task conventional algorithms: [notebooks](https://bitbucket.org/sanjaradylov/nitrocom-learning/src/master/notebooks/).
Install and run [jupyter](https://jupyter.org/install):
```
$ conda install -c conda-forge jupyterlab
$ cd notebooks
$ jupyter notebook
```
Run the notebooks from your browser.
