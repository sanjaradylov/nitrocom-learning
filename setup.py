from setuptools import setup


def readme():
    with open('README.md') as fh:
        return fh.read()


setup(
    name='nitrocomchem',
    version='1.0',
    description=(
        'Multi-output statistical learning on nitroaromatic compounds.'
    ),
    long_description=readme(),
    keywords=[
        'chemistry',
        'deep learning',
        'machine learning',
        'statistical learning',
    ],
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Environment :: Console :: Jupyter',
        'Intended Audience :: Science/Research',
        'License :: Public Domain',
        'Operating System :: POSIX :: Linux',
        'Programming Language :: Python :: 3.5',
        (
            'Topic :: Scientific/Engineering :: Artificial Intelligence :: '
            'Chemistry'
        ),
    ],
    url='https://bitbucket.org/sanjaradylov/nitrocom-learning/',
    author='Sanjar Adylov',
    author_email='',
    license='',
    packages=[
        'nitrocomchem',
    ],
    install_requires=[
        'deepchem',
        'matplotlib',
        'numpy',
        'pandas',
        'scikit-learn',
        'scipy',
        'tensorflow',
    ],
    zip_safe=False,
    include_package_data=True,
)
