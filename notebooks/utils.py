import functools

import matplotlib.pyplot as plt
import numpy as np

from sklearn.model_selection import cross_val_score, KFold, RepeatedKFold
from sklearn.pipeline import Pipeline

from nitrocomchem.utils import root_mean_squared_error, rmse_score


RNG = 0
KFOLD = KFold(n_splits=5, shuffle=True, random_state=RNG)
RKFOLD = RepeatedKFold(n_splits=5, n_repeats=3, random_state=RNG)


def get_estimator_name(estimator):
    if isinstance(estimator, Pipeline):
        return estimator.steps[-1][1].__class__.__name__
    return estimator.__class__.__name__


def print_cv_scores(estimator, X, y, cv=RKFOLD,
                    scoring=root_mean_squared_error,
                    n_jobs=-1, fit_params=None, **msg_kwargs):
    # Get cv results
    scores = cross_val_score(estimator, X, y, cv=cv, scoring=scoring,
                             n_jobs=n_jobs, fit_params=fit_params)

    # Print options
    est_name = msg_kwargs.get('est_name', get_estimator_name(estimator))

    msg = '[{0}] Mean cv score ({1}): '.format(
        est_name, msg_kwargs.get('sc_name', 'RMSE'))
    msg += '{0:.3f} (+/-{1:.3f})'.format(scores.mean(), scores.std())

    print(msg)


def print_train_score(estimator, X, y, scoring=root_mean_squared_error,
                      **msg_kwargs):
    estimator.fit(X, y)
    score = scoring(estimator, X, y)

    est_name = msg_kwargs.get('est_name', get_estimator_name(estimator))
    sc_name = msg_kwargs.get('sc_name', 'RMSE')
    print('[{0}] Train score ({1}): {2:.3f}'.format(est_name, sc_name, score))


def print_single_task_scores(estimator, X, Y,
                             cv=RKFOLD, scoring=None):
    scoring = scoring or functools.partial(rmse_score, multioutput='raw')

    scores = []
    for train_idx, valid_idx in cv.split(X):
        estimator.fit(X.iloc[train_idx], Y[train_idx])

        score = scoring(Y[valid_idx], estimator.predict(X.iloc[valid_idx]))
        scores.append(score)

    mean_scores = np.asarray(scores).mean(axis=0)
    std_scores = np.asarray(scores).std(axis=0)

    print('{{LD50}} Mean cv score (RMSE): {0:.3f} (+/-{1:.3f})'
          .format(mean_scores[0], std_scores[0]))
    print('{{BoilingPoint}} Mean cv score (RMSE): {0:.3f} (+/-{1:.3f})'
          .format(mean_scores[1], std_scores[1]))
    print('{{MeltingPoint}} Mean cv score (RMSE): {0:.3f} (+/-{1:.3f})'
          .format(mean_scores[2], std_scores[2]))


def get_best_features(pipe, X, y, n_best=15,
                      return_features=True, plot_scores=True):
    scores = pipe.named_steps['kbest'].fit(X, y).scores_
    indices = np.argsort(scores)[::-1][:n_best]
    features = X.columns[indices]

    if plot_scores:
        plt.figure(figsize=(14, 5))
        plt.title('Best Features')
        plt.bar(range(n_best), scores[indices])
        plt.xticks(range(n_best), features, rotation=60, ha='right')
        plt.xlim([-1, n_best])
        plt.xlabel('Feature')
        plt.ylabel('MI Score')
        plt.show()

    if return_features:
        return features
